class Product < ActiveRecord::Base
  validates :name, presence: true
  validates :description, presence: true
  VALID_EAN_REGEX = /\d{13}/
  validates :ean, presence: true, length: { maximum:13 },
                  format: { with: VALID_EAN_REGEX },
            uniqueness: true


end
