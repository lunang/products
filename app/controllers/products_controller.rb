class ProductsController < ApplicationController
  def show
    @product = Product.find(params[:id])
  end
  
  def new
    @product = Product.new
  end

  def create
    @product = Product.new(params)
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end
  
  def edit
    @product = Product.find(params[:id])
  end

  def index
    @products = Product.paginate(page:params[:page])
   end
end
