require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup 
    @product = Product.new(
            name: "Paperclip", 
      description: "Large Plain Packof 100",
            ean: "5010255079763")
  end

  test "should be valid" do
    assert @product.valid?
  end

  test "name should be present" do
    @product.name = "   "
    assert_not @product.valid?
  end

  test "description should be present" do
    @product.description = "  "
    assert_not @product.valid?
  end
  
  test "EAN should be present" do
    @product.ean = "   "
    assert_not @product.valid?
  end

  test "EAN validation should reject invalid codes" do
    invalid_eans = %w[121232a adfdf AcR4 123456789] 
    invalid_eans.each do |invalid_ean|
      @product.ean = invalid_ean
      assert_not @product.valid?, "#{invalid_eans.inspect} should be invalid"
    end
  end

  test "EAN should be unique" do
    duplicate_product = @product.dup
    @product.save
    assert_not duplicate_product.valid?
  end
end
