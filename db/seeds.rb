# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Product.create!(name:  "Another Product",
             description: "Description of that product",
             ean:              "4321987654321"
             )

50.times do |n|
  name  = Faker::Name.name
  description = "Product description-#{n+1} "
  ean = rand.to_s[2..14]
  Product.create!(name:  name,
               description: description,
               ean: ean)
end
