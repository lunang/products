class AddIndexToProductsEan < ActiveRecord::Migration
  def change
      add_index :products, :ean, unique: true
  end
end
